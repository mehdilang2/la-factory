package lafactory;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IMaterielFormationRepository;
import lafactory.dao.IMaterielRepository;
import lafactory.model.Materiel;
import lafactory.model.MaterielFormation;
import lafactory.model.Ordinateur;
import lafactory.model.Resolution;
import lafactory.model.Salle;
import lafactory.model.Videoprojecteur;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestUnitMateriel {
	
	private static ClassPathXmlApplicationContext context = null;
	@Autowired
	public IMaterielRepository materielRepo;
	@Test
	public void videoprojecteur() {
		
		
		int startSize = materielRepo.findAll().size();
		
		Videoprojecteur videoprojecteur = new Videoprojecteur();
		videoprojecteur.setCode(200L);
		videoprojecteur.setCoutUtilisation(2.99f);
		videoprojecteur.setHdmi(true);
		videoprojecteur.setResolution(Resolution.UltraHaute);
		videoprojecteur.setVga(false);
		
		videoprojecteur = materielRepo.save(videoprojecteur);
		
		Assert.assertEquals(Long.valueOf(200L), Long.valueOf(videoprojecteur.getCode()));
		Assert.assertEquals(Float.valueOf(2.99f), Float.valueOf(videoprojecteur.getCoutUtilisation()));
		Assert.assertEquals(true, videoprojecteur.getHdmi());
		Assert.assertEquals(false, videoprojecteur.getVga());
		Assert.assertEquals("UltraHaute", videoprojecteur.getResolution().toString());
		
		Optional<Materiel> videoproj = materielRepo.findById(videoprojecteur.getId());
		
		videoprojecteur.setCode(201L);
		videoprojecteur.setCoutUtilisation(3.01f);
		videoprojecteur.setHdmi(false);
		videoprojecteur.setResolution(Resolution.Moyenne);
		videoprojecteur.setVga(true);
		
		videoprojecteur = materielRepo.save(videoprojecteur);
		
		Optional<Materiel> videoproj2 = materielRepo.findById(videoprojecteur.getId());
		
		Assert.assertEquals(Long.valueOf(201L), Long.valueOf(videoprojecteur.getCode()));
		Assert.assertEquals(Float.valueOf(3.01f), Float.valueOf(videoprojecteur.getCoutUtilisation()));
		Assert.assertEquals(false, videoprojecteur.getHdmi());
		Assert.assertEquals(true, videoprojecteur.getVga());
		Assert.assertEquals("Moyenne", videoprojecteur.getResolution().toString());
		
		
		materielRepo.delete(videoprojecteur);

		int endSize = materielRepo.findAll().size();

		Assert.assertEquals(0, endSize - startSize);
		
		}
	
		@BeforeClass
		public static void start() {
			context = new ClassPathXmlApplicationContext("application-context.xml");
		}
		
		@AfterClass
		public static void end() {
			context.close();
		}
		
	
	@Test
	public void ordinateur() {
	int startSize =materielRepo.findAll().size();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	Ordinateur ordi1 = new Ordinateur();
	ordi1.setCode(100L);
	ordi1.setCoutUtilisation(12.5f);
	try {
	ordi1.setDtAchat(sdf.parse("23/07/2019"));
	} catch (ParseException e) {
		e.printStackTrace();
	}
	ordi1.setProcesseur("Intel");
	ordi1.setQuantiteDD(8);
	ordi1.setRam(1);
	
	ordi1=materielRepo.save(ordi1);
	 int middleSize = materielRepo.findAll().size();
	 Assert.assertEquals(1, middleSize - startSize);
	
	Optional<Materiel> ordiOpt=materielRepo.findById(ordi1.getId());
	ordi1=(Ordinateur) ordiOpt.get();
	Assert.assertEquals(Long.valueOf(100L),Long.valueOf( ordi1.getCode()));
	Assert.assertEquals(Float.valueOf(12.5F),Float.valueOf( ordi1.getCoutUtilisation()));
	Assert.assertEquals("Intel",ordi1.getProcesseur());
	Assert.assertEquals(8, ordi1.getQuantiteDD());
	Assert.assertEquals(1, ordi1.getRam());
	try {
		Assert.assertEquals(sdf.parse("23/07/2019"), ordi1.getDtAchat());
	} catch (ParseException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	ordi1.setCode(10l);
	ordi1.setCoutUtilisation(15.2f);
	try {
		ordi1.setDtAchat(sdf.parse("26/05/2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	ordi1.setProcesseur("AMD");
	ordi1.setQuantiteDD(16);
	ordi1.setRam(12);
	
	ordi1=materielRepo.save(ordi1);
	ordiOpt=materielRepo.findById(ordi1.getId());
	ordi1=(Ordinateur) ordiOpt.get();
	
	Assert.assertEquals(Long.valueOf(10L),Long.valueOf( ordi1.getCode()));
	Assert.assertEquals(Float.valueOf(15.2F),Float.valueOf( ordi1.getCoutUtilisation()));
	Assert.assertEquals("AMD",ordi1.getProcesseur());
	Assert.assertEquals(16, ordi1.getQuantiteDD());
	Assert.assertEquals(12, ordi1.getRam());
	try {
		Assert.assertEquals(sdf.parse("26/05/2019"), ordi1.getDtAchat());
	} catch (ParseException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	materielRepo.delete(ordi1);
	int endSize= materielRepo.findAll().size();
	Assert.assertEquals(0, endSize - startSize);
	
	}
	@Test
	public void salle() {
	int startSize =materielRepo.findAll().size();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	Salle salle1= new Salle();
	salle1.setPlaceMax(13);
	salle1.setCode(25L);
	salle1.setCoutUtilisation(56.2f);
	
	salle1=materielRepo.save(salle1);
	 int middleSize = materielRepo.findAll().size();
	 Assert.assertEquals(1, middleSize - startSize);
	
	Optional<Materiel> salleOpt=materielRepo.findById(salle1.getId());
	salle1=(Salle) salleOpt.get();
	Assert.assertEquals(Long.valueOf(25L),Long.valueOf( salle1.getCode()));
	Assert.assertEquals(Float.valueOf(56.2f),Float.valueOf(salle1.getCoutUtilisation()));
	Assert.assertEquals(13, salle1.getPlaceMax());
	
	salle1.setCode(10l);
	salle1.setCoutUtilisation(15.2f);
	salle1.setPlaceMax(15);
	
	salle1=materielRepo.save(salle1);
	salleOpt=materielRepo.findById(salle1.getId());
	salle1=(Salle) salleOpt.get();
	
	Assert.assertEquals(Long.valueOf(10L),Long.valueOf( salle1.getCode()));
	Assert.assertEquals(Float.valueOf(15.2F),Float.valueOf( salle1.getCoutUtilisation()));
	Assert.assertEquals(15, salle1.getPlaceMax());
	materielRepo.delete(salle1);
	int endSize= materielRepo.findAll().size();
	Assert.assertEquals(0, endSize - startSize);
	
	}
	@Autowired
	IMaterielFormationRepository materielFormationRepo;
	@Test
	public void dispoMateriel() {
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		MaterielFormation materielFormation1=new MaterielFormation();
		try {
			materielFormation1.setDtDebut(sdf.parse("10/05/2019"));
			materielFormation1.setDtFin(sdf.parse("09/09/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		materielFormation1=materielFormationRepo.save(materielFormation1);
		Optional<MaterielFormation> materielFormationOpt=materielFormationRepo.findById(materielFormation1.getId());
		materielFormation1=materielFormationOpt.get();
		try {
			Assert.assertEquals(sdf.parse("10/05/2019"),materielFormation1.getDtDebut());
			Assert.assertEquals(sdf.parse("09/09/2019"),materielFormation1.getDtFin());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Salle salle1= new Salle();
		salle1.setPlaceMax(13);
		salle1.setCode(25L);
		salle1.setCoutUtilisation(56.2f);
		
		salle1=materielRepo.save(salle1);
		materielFormation1.setMateriel(salle1);
		materielFormation1=materielFormationRepo.save(materielFormation1);
		Materiel materiel2 =null ;
		List<Materiel> materiels=null;
		try {
			 materiels= materielRepo.findAllMaterielDispoByDate(sdf.parse("11/10/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Materiel materiel:materiels) {
			materiel2=materiel;
			Assert.assertEquals(Long.valueOf(25L), materiel2.getCode());
		}
		
		
		
		
	}
}
