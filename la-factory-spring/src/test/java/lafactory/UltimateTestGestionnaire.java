package lafactory;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IGestionnaireRepository;
import lafactory.model.Civilite;
import lafactory.model.Formation;
import lafactory.model.Gestionnaire;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class UltimateTestGestionnaire {
	
	@Autowired
	private IGestionnaireRepository gestionnaireRepo;
	
	@Test
	public void gestionnaire() {
		
		int startSize = gestionnaireRepo.findAll().size();
		
		Gestionnaire gestionnaire1 = new Gestionnaire();
		gestionnaire1.setCivilite(Civilite.M);
		gestionnaire1.setNom("nomdugestionnaire");
		gestionnaire1.setPrenom("prenomdugestionnaire");
		gestionnaire1.setTelephone("0685487985");
	//TODO	gestionnaire1.setFormations(formations);
		
		gestionnaire1 = gestionnaireRepo.save(gestionnaire1);
		Assert.assertEquals("nomdugestionnaire", gestionnaire1.getNom());
		
		gestionnaire1.setNom("newnomdugestionnaire");
		gestionnaire1 = gestionnaireRepo.save(gestionnaire1);
		Assert.assertEquals("newnomdugestionnaire", gestionnaire1.getNom());
		
		int middleSize = gestionnaireRepo.findAll().size();
		Assert.assertEquals(1, middleSize - startSize);
		
		gestionnaireRepo.delete(gestionnaire1);
		
		int endSize = gestionnaireRepo.findAll().size();
		Assert.assertEquals(0, endSize - startSize);
		
		Formation formation1 = new Formation();
		
		formation1.setGestionnaire(gestionnaire1);	
	    //TODO	gestionnaire1.getFormations.getNom();
		
		
	}

}
