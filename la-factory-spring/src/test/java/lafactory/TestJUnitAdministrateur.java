package lafactory;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IPersonneRepository;
import lafactory.dao.IUtilisateurRepository;
import lafactory.model.Administrateur;
import lafactory.model.Adresse;
import lafactory.model.Civilite;
import lafactory.model.Personne;
import lafactory.model.TypeUser;
import lafactory.model.Utilisateur;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestJUnitAdministrateur {

	@Autowired
	private IPersonneRepository personneRepo;
	@Autowired
	private IUtilisateurRepository uilisateurRepo;

	@Test
	public void administrateur() {

		Adresse adrAdmin = new Adresse();
		adrAdmin.setVoie("404 rue de l'Internet");
		adrAdmin.setVille("Router");
		adrAdmin.setPays("France");
		adrAdmin.setCodePostal("12345");
		
		Utilisateur userJohn = new Utilisateur();
		userJohn.setTypeUser(TypeUser.Administrateur);
		userJohn.setMail("john.doe@mail.com");
		userJohn.setMotDePasse("JohnDoe");
		userJohn = uilisateurRepo.save(userJohn);

		Administrateur admin = new Administrateur();
		admin.setNom("ADMIN");
		admin.setPrenom("Admin");
		admin.setCivilite(Civilite.NSP);
		admin.setTelephone("0662457896");
		admin.setAdresse(adrAdmin);
		admin.setUtilisateur(userJohn);

		admin = personneRepo.save(admin);

		Optional<Personne> optAdmin = personneRepo.findById(admin.getId());

		Assert.assertEquals("ADMIN", optAdmin.get().getNom());
		Assert.assertEquals("Admin", optAdmin.get().getPrenom());
		Assert.assertEquals("404 rue de l'Internet", optAdmin.get().getAdresse().getVoie());
		
		admin = (Administrateur) optAdmin.get();
		
		adrAdmin.setVoie("405 rue de l'Internet");
		adrAdmin.setVille("FAI");
		adrAdmin.setPays("Espagne");
		adrAdmin.setCodePostal("54321");
		
		userJohn.setTypeUser(TypeUser.Administrateur);
		userJohn.setMail("marcel@mail.com");
		userJohn.setMotDePasse("MarcelDoe");
		userJohn = uilisateurRepo.save(userJohn);
		
		admin.setNom("ADIN");
		admin.setPrenom("Adin");
		admin.setCivilite(Civilite.M);
		admin.setTelephone("0662457896");
		admin.setAdresse(adrAdmin);
		admin.setUtilisateur(userJohn);
		
		admin = personneRepo.save(admin);

		optAdmin = personneRepo.findById(admin.getId());

		Assert.assertEquals("ADIN", optAdmin.get().getNom());
		Assert.assertEquals("Adin", optAdmin.get().getPrenom());
		Assert.assertEquals("405 rue de l'Internet", optAdmin.get().getAdresse().getVoie());
		
//		List<Utilisateur> utilisateurs =  uilisateurRepo.findUtilisateursByType(TypeUser.Administrateur);
//		
//		for (Utilisateur utilisateur : utilisateurs) {
//			System.out.println(utilisateur.getMail());
//		}

	}

}
