package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IFormationRepository;
import lafactory.dao.IModuleRepository;
import lafactory.model.Formation;
import lafactory.model.Module;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestUnitaireModule {

	@Autowired
	IModuleRepository moduleRepo;
	@Autowired
	IFormationRepository formationRepo;

	@Test
	public void Module() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Formation antoinette = new Formation();
		antoinette.setCapacite(12);
		antoinette.setIntitule("FORMATION JAVA POEI");
		try {
			antoinette.setDtDebut(sdf.parse("19/06/2019"));
			antoinette.setDtFin(sdf.parse("20/09/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		antoinette = formationRepo.save(antoinette);
		
		
		
//		Module javaEE = new Module();
//		try {
//			javaEE.setDtDebut(sdf.parse("19/06/2019"));
//			javaEE.setDtFin(sdf.parse("28/06/2019"));
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		// javaEE.setFormateur(formateur);
//		javaEE.setFormation(antoinette);
//		// javaEE.setMatiere(matiere);
//
//		javaEE = moduleRepo.save(javaEE);
//
//		Optional<Module> javaEEOptional = moduleRepo.findById(javaEE.getId());
//		
//		Assert.assertEquals("19/06/2019", sdf.format(javaEEOptional.get().getDtDebut()));
//		Assert.assertEquals("28/06/2019", sdf.format(javaEEOptional.get().getDtFin()));
//		
//		System.out.println(javaEEOptional.get().getDtDebut());
//		
//		
//		Module javaEEOptionalDate = moduleRepo.findModuleByDate(sdf.parse("19/06/2019"), sdf.parse("28/06/2019"));
//		
//		System.out.println(javaEEOptionalDate.getFormation().getIntitule());
//		
//		Assert.assertEquals("FORMATION JAVA POEI", javaEEOptionalDate.getFormation().getIntitule());
		
//		moduleRepo.findModuleByFormation(formationid);
//		
//		moduleRepo.findByIdWithFormateur(id);
	}

}
