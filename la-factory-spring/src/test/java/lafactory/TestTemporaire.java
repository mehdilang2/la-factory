package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IClientFormationRepository;
import lafactory.dao.IClientRepository;
import lafactory.dao.IFormationRepository;
import lafactory.dao.IMaterielFormationRepository;
import lafactory.dao.IMaterielRepository;
import lafactory.dao.IMatiereRepository;
import lafactory.dao.IModuleRepository;
import lafactory.dao.IPersonneRepository;
import lafactory.dao.IUtilisateurRepository;
import lafactory.model.Adresse;
import lafactory.model.MaterielFormation;
import lafactory.model.Ordinateur;
import lafactory.model.Resolution;
import lafactory.model.Salle;
import lafactory.model.Videoprojecteur;

	@RunWith(SpringJUnit4ClassRunner.class)
	@ContextConfiguration(locations = "/application-context.xml")
	public class TestTemporaire {
		
		@Autowired
		IMaterielRepository materielRepo;
		@Autowired
		IClientFormationRepository clientFormationRepo;
		@Autowired
		IClientRepository clientRepo;
		@Autowired
		IFormationRepository formationRepo;
		@Autowired
		IModuleRepository moduleRepo;
		@Autowired
		IMatiereRepository matiereRepo;
		@Autowired
		IPersonneRepository personneRepo;
		@Autowired
		IUtilisateurRepository utilisateurRepo;
		@Autowired
		IMaterielFormationRepository materielFormationRepo;
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		@Test
		public void materiel() {
			
			Salle salle1 = new Salle();
			salle1.setPlaceMax(13);
			salle1.setCode(25L);
			salle1.setCoutUtilisation(56.2f);
			salle1 = (Salle) materielRepo.save(salle1);
			
			//Assert.assertEquals
			
			Salle salle2 = new Salle();
			salle2.setPlaceMax(50);
			salle2.setCode(26L);
			salle2.setCoutUtilisation(100.99f);
			salle2 = (Salle) materielRepo.save(salle2);
			
			Ordinateur ordi1 = new Ordinateur();
			ordi1.setCode(100L);
			ordi1.setCoutUtilisation(12.5f);
			try {
				ordi1.setDtAchat(sdf.parse("23/07/2019"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			ordi1.setProcesseur("Intel");
			ordi1.setQuantiteDD(8);
			ordi1.setRam(1);
			ordi1 = (Ordinateur) materielRepo.save(ordi1);
			
			Videoprojecteur video = new Videoprojecteur();
			video.setCode(200L);
			video.setCoutUtilisation(2.99f);
			video.setHdmi(true);
			video.setVga(false);
			video.setResolution(Resolution.UltraHaute);
			video = (Videoprojecteur) materielRepo.save(video);
			
			Adresse adresse = new Adresse();
			adresse.setVoie("86 rue J.F. Kennedy");
			adresse.setVille("Merignac");
			adresse.setCodePostal("33700");
			adresse.setPays("France");
			
			salle1.setAdresse(adresse);
			salle1 = (Salle) materielRepo.save(salle2);
			
			MaterielFormation materielformation = new MaterielFormation();
			try {
				materielformation.setDtDebut(sdf.parse("18/06/2019"));
				materielformation.setDtDebut(sdf.parse("20/09/2019"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			materielformation = materielFormationRepo.save(materielformation);
		}
		
		@Test
		public void clientFormation() {
			
		}
		
		@Test
		public void client() {
			
		}
		
		@Test
		public void formation() {
			
		}
		
		@Test
		public void module() {
			
		}
		
		@Test
		public void matiere() {
			
		}
		
		@Test
		public void personne() {
			
		}
		
		@Test
		public void utilisateur() {
			
		}
		
		@Test
		public void materielFormation() {
			
		} 
		
	}