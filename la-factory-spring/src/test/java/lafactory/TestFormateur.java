package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IMatiereRepository;
import lafactory.dao.IPersonneRepository;
import lafactory.model.Adresse;
import lafactory.model.Civilite;
import lafactory.model.Disponibilite;
import lafactory.model.Formateur;
import lafactory.model.Personne;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestFormateur {
	
	@Autowired
	private IPersonneRepository personneRepo;
	@Autowired
	private IMatiereRepository matiereRepo;
	
	@Test
	public void Formateur() {
		SimpleDateFormat spd = new SimpleDateFormat("dd/MM/yyyy");
		
		Adresse adresse1 = new Adresse();
		adresse1.setVoie("86 rue J.F. Kennedy");
		adresse1.setVille("Merignac");
		adresse1.setCodePostal("33700");
		adresse1.setPays("France");
		
		Disponibilite dispo1 = new Disponibilite();
		try {
			dispo1.setDtDispo(spd.parse("28/07/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int startSize = personneRepo.findAll().size();
		
//		Matiere java = new Matiere();
//		java.setTitre("JAVA");
//		java = matiereRepo.save(java);
		
//		List<Matiere> matieres = new ArrayList<Matiere>();
//		
//		matieres.add(java);
		
		Formateur formateur1 =new Formateur();
		formateur1.setAdresse(adresse1);
		formateur1.setCivilite(Civilite.M);
		formateur1.setNom("SULTAN");
		formateur1.setPrenom("Eric");
		formateur1.setTelephone("0617231458");
//		formateur1.setMatieres(matieres);
		formateur1.setDisponibilite(dispo1);
//		formateur1.getMatieres().add(java);
//		formateur1.getModules().add(module1);
//		formateur1.setUtilisateur(utilisateur1);
		
		formateur1 = (Formateur) personneRepo.save(formateur1);
		
		Optional <Personne> f1 = null;
		f1 = personneRepo.findById(formateur1.getId());	
		formateur1 = (Formateur) f1.get();
		
		Assert.assertEquals(Civilite.M, formateur1.getCivilite());
		Assert.assertEquals("86 rue J.F. Kennedy", formateur1.getAdresse().getVoie());
		Assert.assertEquals("SULTAN", formateur1.getNom());
		Assert.assertEquals("Eric", formateur1.getPrenom());
		Assert.assertEquals("0617231458", formateur1.getTelephone());
		Assert.assertEquals("28/07/2019", spd.format(formateur1.getDisponibilite().getDtDispo()));
		
		formateur1.setNom("BRUEL");
		formateur1.setPrenom("Patrick");
		
		formateur1 = (Formateur) personneRepo.save(formateur1);
		
		
		f1 = personneRepo.findById(formateur1.getId());	

		formateur1 = (Formateur) f1.get();

//		List<Formateur> formateurs = new ArrayList<Formateur>();
//		formateurs = formateurRepo.findAllFormateurByTitreMatiere("JAVA");
//		
//		for(Formateur form : formateurs) {
//			System.out.println(form.getNom());
//		}

		Assert.assertEquals(Civilite.M, formateur1.getCivilite());
		Assert.assertEquals("86 rue J.F. Kennedy", formateur1.getAdresse().getVoie());
		Assert.assertEquals("BRUEL", formateur1.getNom());
		Assert.assertEquals("Patrick", formateur1.getPrenom());
		Assert.assertEquals("0617231458", formateur1.getTelephone());
		Assert.assertEquals("28/07/2019", spd.format(formateur1.getDisponibilite().getDtDispo()));
		
		int middleSize = personneRepo.findAll().size();
		
		Assert.assertEquals(1, middleSize - startSize);
		
		personneRepo.delete(formateur1);
		
		int endSize = personneRepo.findAll().size();

		Assert.assertEquals(0, endSize - startSize);
		
		
		
		
	}
	

}
