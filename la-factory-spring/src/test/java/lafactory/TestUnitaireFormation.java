package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IFormationRepository;
import lafactory.dao.IPersonneRepository;
import lafactory.model.Civilite;
import lafactory.model.Formation;
import lafactory.model.Stagiaire;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestUnitaireFormation {

	private IFormationRepository formationRepository;
	private IPersonneRepository personneRepository;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	@Test
	public void formation() {

		Formation antoinette = new Formation();
		antoinette.setCapacite(12);
		antoinette.setIntitule("FORMATION JAVA POEI");
		try {
			antoinette.setDtDebut(sdf.parse("19/06/2019"));
			antoinette.setDtFin(sdf.parse("20/09/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		antoinette = formationRepository.save(antoinette);

		Optional <Formation> antoinetteTest = formationRepository.findById(antoinette.getId());
		Assert.assertEquals("Paris", antoinetteTest.get().getIntitule());
		
		Stagiaire camille = new Stagiaire();
		camille.setCivilite(Civilite.MLLE);
//		camille.setAdresse(adresseCamille);
//		camille.setClient(sopra);
		camille.setCommentaires("débutant");
//		camille.setFormation(menagerie);
		camille.setNom("Saphy");
		camille.setPrenom("Camille");
		camille.setTelephone("0617227456");

		camille = (Stagiaire)personneRepository.save(camille);
		
		antoinette = formationRepository.findFormationbyNomStagiaire(camille.getNom());
		
	}

}
