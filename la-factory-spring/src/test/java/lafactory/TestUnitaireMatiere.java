package lafactory;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lafactory.dao.IMatiereRepository;
import lafactory.model.Formateur;
import lafactory.model.Matiere;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class TestUnitaireMatiere {
	
	@Autowired
	private IMatiereRepository matiereRepo;
	
	public void matiere() {
		
		Matiere javaDebutant = new Matiere();
		Formateur philippe = new Formateur();
		
		philippe.setNom("GADJO");
		philippe.setPrenom("Philippe");
		
		javaDebutant.setTitre("JAVA Débutant");
		javaDebutant.setDuree(3); // Jour ou mois??
		javaDebutant.setObjectifs("Apprendre à programmer les bases de JAVA");
		javaDebutant.setPrerequis("Aucun");
		javaDebutant.setContenu("Présentation JAVA8 \n Outils de développement \n Création de classes");
		
		matiereRepo.save(javaDebutant);
		
		Assert.assertEquals("JAVA Débutant", javaDebutant.getTitre());
		Assert.assertEquals((Integer)3, javaDebutant.getDuree());
		Assert.assertEquals("Apprendre à programmer les bases de JAVA", javaDebutant.getObjectifs());
		Assert.assertEquals("Aucun",javaDebutant.getPrerequis());
		Assert.assertEquals("Présentation JAVA8 \n Outils de développement \n Création de classes",javaDebutant.getContenu());
		
//		List<Matiere> matieres1 = new ArrayList<Matiere>();
//		matieres1 = matiereRepo.findByIdMatiereWithFormateur(philippe.getId());
	
//		List<Matiere> matieres2 = new ArrayList<Matiere>();
//		matieres2 = matiereRepo.findByIdMatiereWithNomAndPrenomFormateur(philippe.getNom(), philippe.getPrenom());		
		
		
	}

}
