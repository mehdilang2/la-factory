package lafactory.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import lafactory.model.Matiere;

public interface IMatiereRepository extends JpaRepository<Matiere, Long>{
	
	//Matiere findByMatiere(String titre);
	
	@Query("select m from Matiere m left join fetch m.formateur f where f.id = :monId")
	List <Matiere> findByIdMatiereWithFormateur(@Param("monId") Long id);
	
//	@Query("select m from Matiere m left join fetch m.formateur f where f.nom = :nom and f.prenom = :prenom")
//	List <Matiere> findByIdMatiereWithNomAndPrenomFormateur(@Param("nom") String nom, @Param("prenom") String prenom);
//	
//	@Query("select m from Matiere m left join fetch m.module mo where mo.id = :monId")
//	Matiere findByIdWithModule(@Param("monId") Long id);
}
