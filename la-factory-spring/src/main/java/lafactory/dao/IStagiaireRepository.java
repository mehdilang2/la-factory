package lafactory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import lafactory.model.Formateur;

public interface IStagiaireRepository extends JpaRepository<Formateur, Long>{


}