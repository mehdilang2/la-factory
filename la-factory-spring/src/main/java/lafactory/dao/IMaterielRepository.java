package lafactory.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import lafactory.model.Materiel;
import lafactory.model.Ordinateur;
import lafactory.model.Salle;
import lafactory.model.Videoprojecteur;

public interface IMaterielRepository extends JpaRepository<Materiel,Long>{
	@Query("select o from Ordinateur o")
    List<Ordinateur> findAllOrdinateur();
    @Query("select s from Salle s")
    List<Salle> findAllSalle();
    @Query("select v from Videoprojecteur v")
    List<Videoprojecteur> findAllVideoprojecteur();
    //@Query("select m from Materiel m left join m.materielFormation mf left join mf.formation f where f.id = :monId")
    //List<Materiel> findAllMaterielfromFormationId(@Param("monId") Long id);
    
    //@Query("select o from Ordinateur o left join o.stagiaire s where s.id = :monId")
    //List<Materiel> findAllMaterielfromStagiaireId(@Param("monId") Long id);
    
    @Query("select o from Ordinateur o left join o.stagiaire s where s.nom = :monNom and s.prenom = :monPrenom")
    List<Materiel> findAllMaterielfromStagiaireNomAndPrenom(@Param("monNom") String nom, @Param ("monPrenom") String prenom);
    
    //@Query("select m from Materiel m left join m.materielFormation mf where mf.dtDebut =< :maDate and mf.dtFin => :maDate")
    //List<Materiel> findAllMaterielIndispoByDate(@Param("maDate") Date date);
    
    @Query("select m from Materiel m left join m.materielFormation mf where mf.dtDebut > :maDate and mf.dtFin < :maDate")
    List<Materiel> findAllMaterielDispoByDate(@Param("maDate") Date date);
    
    @Query("select m from Materiel m left join m.materielFormation mf left join mf.formation f where f.intitule = :monIntitule")
    List<Materiel> findMaterielfromFormationIntitule(@Param("monIntitule") String intitule);
}
