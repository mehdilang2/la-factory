package lafactory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import lafactory.model.Administrateur;

public interface IAdministrateurRepository extends JpaRepository <Administrateur,Long>{

}
