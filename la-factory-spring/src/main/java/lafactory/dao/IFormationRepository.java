package lafactory.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import lafactory.model.Formation;


public interface IFormationRepository extends JpaRepository<Formation, Long>{

	
	@Query("select f from Formation f left join fetch f.stagiaire s where f.nom = :nomStagiaire")
	Formation findFormationbyNomStagiaire(@Param("nomStagiaire") String nom);	
	
	@Query("select f from Formation f left join fetch f.client c where f.id = :idClient")
	List<Formation> findFormationbyIdClient(@Param("idClient") Long id);

	@Query("select f from Formation f left join fetch f.module m where f.id = :idModule")
	Formation findFormationbyIdModule(@Param("idModule") Long id);
	
	@Query("select f from Formation f left join fetch f.materiel m where f.id = :idMateriel")
	List<Formation> findFormationbyIdMateriel(@Param("idMateriel") Long id);
	
	@Query("select f from Formation f left join fetch f.gestionnaire g where f.nom = :nomGestionnaire")
	List<Formation> findFormationbyNomGestionnaire(@Param("nomGestionnaire") String nom);
}
