package lafactory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import lafactory.model.Client;

public interface IClientRepository extends JpaRepository<Client,Long>{

}
