package lafactory.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import lafactory.model.Administrateur;
import lafactory.model.Contact;
import lafactory.model.Formateur;
import lafactory.model.Gestionnaire;
import lafactory.model.Personne;
import lafactory.model.Stagiaire;

public interface IPersonneRepository extends JpaRepository<Personne, Long>{
	
	@Query("select a from Administrateur a")
	List<Administrateur> findAllAdministrateur();
	@Query("select c from Contact c")
	List<Contact> findAllContact();
	@Query("select f from Formateur f")
	List<Formateur> findAllFormateur();
	@Query("select g from Gestionnaire g")
	List<Gestionnaire> findAllGestionnaire();
	@Query("select s from Stagiaire s")
	List<Stagiaire> findAllStagiaire();
	@Query ("select g from Gestionnaire g left join fetch g.formations f where f.id = :monid" )
	List<Gestionnaire> findAllGestionnaireByFormationId(@Param("monid") Long id);
//TODO TEST	@Query ("select g from Gestionnaire g left join fetch g.formations f left join fetch f.client c where c.id = :monid" )
	List<Gestionnaire> findAllGestionnaireByClientId(@Param("monid") Long id);
//TODO TEST	@Query ("select g from Gestionnaire g left join fetch g.formations f left join fetch f.client c where c.nomEntreprise = :monnom" )
	List<Gestionnaire> findAllGestionnaireByClientnomEntreprise(@Param("monnom") Long id);
	

	@Query ("from Formateur where matiere = :maMatiere")
	List<Formateur> findAllByMatiere (@Param("maMatiere") String matiere);
	@Query ("select f from Formateur f left join f.matieres m where m.titre = :titreMatiere")
	List<Formateur>  findAllFormateurByTitreMatiere (@Param("titreMatiere") String titre);
}
