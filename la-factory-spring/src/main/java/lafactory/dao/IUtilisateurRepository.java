package lafactory.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import lafactory.model.TypeUser;
import lafactory.model.Utilisateur;

public interface IUtilisateurRepository extends JpaRepository<Utilisateur, Long>{

	@Query("select u from Utilisateur u")
	List<Utilisateur> findAllUtilisateurs();
	
	@Query("select u from Utilisateur u where u.typeUser = :typeUser")
	List<Utilisateur> findUtilisateursByType(@Param("typeUser") TypeUser typeUser);
	
}
