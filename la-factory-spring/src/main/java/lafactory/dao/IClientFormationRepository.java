package lafactory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import lafactory.model.ClientFormation;

public interface IClientFormationRepository extends JpaRepository<ClientFormation, Long>{

}
