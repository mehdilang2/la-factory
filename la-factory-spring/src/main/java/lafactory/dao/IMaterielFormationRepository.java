package lafactory.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import lafactory.model.MaterielFormation;

public interface IMaterielFormationRepository extends JpaRepository<MaterielFormation,Long> {

}
