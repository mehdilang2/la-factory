package lafactory.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Formation")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Formation {
	
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "date_debut", length = 100)
	@Temporal(TemporalType.DATE)
	private Date dtDebut;

	@Column(name = "date_fin", length = 100)
	@Temporal(TemporalType.DATE)
	private Date dtFin;	

	private String intitule;

	private Integer capacite;
	
	@OneToMany(mappedBy = "formation")
	private List<MaterielFormation> materielFormation = new ArrayList<MaterielFormation>();
	
	@OneToMany(mappedBy = "formation")
	private List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();

	@OneToMany(mappedBy = "formation")
	private List<ClientFormation> clientFormations = new ArrayList<ClientFormation>();
	
	@OneToMany(mappedBy = "formation")
	private List<Module> modules = new ArrayList<Module>();
	
	@ManyToOne
	@JoinColumn(name="gestionnaire")
	private Gestionnaire gestionnaire;
	
	
	public Formation() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDtDebut() {
		return dtDebut;
	}
	public void setDtDebut(Date dtDebut) {
		this.dtDebut = dtDebut;
	}
	public Date getDtFin() {
		return dtFin;
	}
	public void setDtFin(Date dtFin) {
		this.dtFin = dtFin;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public Integer getCapacite() {
		return capacite;
	}
	public void setCapacite(Integer capacite) {
		this.capacite = capacite;
	}
	public List<MaterielFormation> getMaterielFormation() {
		return materielFormation;
	}
	public void setMaterielFormation(List<MaterielFormation> materielFormation) {
		this.materielFormation = materielFormation;
	}
	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}
	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}
	public List<ClientFormation> getClientFormations() {
		return clientFormations;
	}
	public void setClientFormations(List<ClientFormation> clientFormations) {
		this.clientFormations = clientFormations;
	}
	public List<Module> getModules() {
		return modules;
	}
	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
	public Gestionnaire getGestionnaire() {
		return gestionnaire;
	}
	public void setGestionnaire(Gestionnaire gestionnaire) {
		this.gestionnaire = gestionnaire;
	}
	
	
}
