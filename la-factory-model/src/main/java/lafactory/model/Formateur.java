package lafactory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;

@Entity
public class Formateur extends Personne {

	@OneToMany(mappedBy = "formateur")
	private List<Module> modules = new ArrayList<Module>();

	@ManyToMany
	@JoinTable(name = "formateur_matiere", uniqueConstraints = @UniqueConstraint(columnNames = { "formateur_id",
			"matiere_id" }), joinColumns = @JoinColumn(name = "formateur_id"), inverseJoinColumns = @JoinColumn(name = "matiere_id"))
	private List<Matiere> matieres = new ArrayList<Matiere>();

	@Embedded
	private Disponibilite disponibilite;

	public Formateur() {
		super();
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<Matiere> matieres) {
		this.matieres = matieres;
	}

	public Disponibilite getDisponibilite() {
		return disponibilite;
	}

	public void setDisponibilite(Disponibilite disponibilite) {
		this.disponibilite = disponibilite;
	}

}
