package lafactory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Client")
public class Client {

	@Id
	@GeneratedValue
	private long id;
	@Column(name = "NOM_ENTREPRISE", length = 50)
	private String nomEntreprise;
	@Column(name = "NUMERO_SIRET", length = 50)
	private String numeroSiret;
	@Enumerated(EnumType.ORDINAL)
	private TypeEntreprise typeEntreprise;
	@Column(name = "NUMERO_TVA", length = 50)
	private String numTva;
	@Embedded
	private Adresse adresse;
	@OneToMany(mappedBy = "client")
	private List<Contact> contacts = new ArrayList<Contact>();
	@OneToMany(mappedBy = "client")
	private List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();
	@OneToMany(mappedBy = "client")
	private List<ClientFormation> clientFormations = new ArrayList<ClientFormation>();
	
	public Client() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

	public String getNumTva() {
		return numTva;
	}

	public void setNumTva(String numTva) {
		this.numTva = numTva;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}

	public List<ClientFormation> getClientFormations() {
		return clientFormations;
	}

	public void setClientFormations(List<ClientFormation> clientFormations) {
		this.clientFormations = clientFormations;
	}
	
	

}
