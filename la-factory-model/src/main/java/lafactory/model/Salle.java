package lafactory.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("Salle")
public class Salle extends Materiel {
	
	
	private int placeMax;
	@Embedded
	private Adresse adresse;

	public int getPlaceMax() {
		return placeMax;
	}

	public void setPlaceMax(int placeMax) {
		this.placeMax = placeMax;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	

}
