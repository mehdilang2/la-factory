package lafactory.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class Disponibilite implements Serializable {

	private Date dtDispo;

	public Disponibilite() {
		super();
	}

	public Date getDtDispo() {
		return dtDispo;
	}

	public void setDtDispo(Date dtDispo) {
		this.dtDispo = dtDispo;
	}

}
