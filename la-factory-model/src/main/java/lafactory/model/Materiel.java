package lafactory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;




@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="Type_materiel")
public abstract class Materiel {

	@Id
	@GeneratedValue
	private Long id;
	
	private Long code;
		
	private Float coutUtilisation;
	@OneToMany(mappedBy = "materiel")
	private List<MaterielFormation> materielFormation = new ArrayList<MaterielFormation>();
	
	
	

	public Long getCode() {
		return code;
	}
	public void setCode(Long code) {
		this.code = code;
	}
	public Float getCoutUtilisation() {
		return coutUtilisation;
	}
	public void setCoutUtilisation(Float coutUtilisation) {
		this.coutUtilisation = coutUtilisation;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
}
