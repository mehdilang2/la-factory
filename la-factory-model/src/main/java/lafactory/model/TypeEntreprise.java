package lafactory.model;

public enum TypeEntreprise {
	
	SARL("Société à Responsabilité Limitée"), SA("Société Anonyme"), SAS("Société par Actions Limitée");
	
	private final String label;
	
	private TypeEntreprise(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

}
