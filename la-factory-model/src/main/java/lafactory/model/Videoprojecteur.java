package lafactory.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("video_projecteur")
public class Videoprojecteur extends Materiel {

	private Boolean hdmi;
	private Boolean vga;
	@Enumerated(EnumType.ORDINAL)
	private Resolution resolution;
	
	
	public Boolean getHdmi() {
		return hdmi;
	}
	public void setHdmi(Boolean hdmi) {
		this.hdmi = hdmi;
	}
	public Boolean getVga() {
		return vga;
	}
	public void setVga(Boolean vga) {
		this.vga = vga;
	}
	public Resolution getResolution() {
		return resolution;
	}
	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}
	
	
}
