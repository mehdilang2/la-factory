package lafactory.model;

import java.util.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("ordinateur")
public class Ordinateur extends Materiel{
	
	private String processeur;
	private int ram;
	private int quantiteDD;
	private Date dtAchat;
	@OneToMany(mappedBy = "ordinateur")
	private  List<Stagiaire> stagiaire;
	
	public String getProcesseur() {
		return processeur;
	}
	public void setProcesseur(String processeur) {
		this.processeur = processeur;
	}
	public int getRam() {
		return ram;
	}
	public void setRam(int ram) {
		this.ram = ram;
	}
	public int getQuantiteDD() {
		return quantiteDD;
	}
	public void setQuantiteDD(int quantiteDD) {
		this.quantiteDD = quantiteDD;
	}
	public Date getDtAchat() {
		return dtAchat;
	}
	public void setDtAchat(Date dtAchat) {
		this.dtAchat = dtAchat;
	}
	public List<Stagiaire> getStagiaire() {
		return stagiaire;
	}
	public void setStagiaire(List<Stagiaire> stagiaire) {
		this.stagiaire = stagiaire;
	}
	
	
	

}
