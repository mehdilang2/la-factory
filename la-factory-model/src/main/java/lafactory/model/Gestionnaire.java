package lafactory.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Gestionnaire extends Personne{
	@OneToMany(mappedBy = "gestionnaire")
	private List<Formation> formations = new ArrayList<Formation>();

	public Gestionnaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<Formation> getFormations() {
		return formations;
	}

	public void setFormations(List<Formation> formations) {
		this.formations = formations;
	}
	
}
