package lafactory.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Contact extends Personne {
	@ManyToOne
	@JoinColumn(name = "id_client")
	private Client client;

	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}

}
