package lafactory.dao.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import lafactory.Singleton;
import lafactory.dao.IModuleDao;
import lafactory.model.Formateur;
import lafactory.model.Formation;
import lafactory.model.Module;




public class ModuleDaoJpa implements IModuleDao{

	@Override
	public List<Module> findAll() {
		List<Module> modules = new ArrayList<Module>();

		EntityManager em = null;
		EntityTransaction tx = null;

		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			TypedQuery<Module> query = em.createQuery("select m from Module m", Module.class);

			modules = query.getResultList();

			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}

		return modules;
	}

	@Override
	public Module findById(Long id) {
		Module obj = null;

		EntityManager em = null;
		EntityTransaction tx = null;

		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			obj = em.find(Module.class, id);

			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}

		return obj;
	}

	@Override
	public Module save(Module obj) {
		EntityManager em = null;
		EntityTransaction tx = null;

		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			obj = em.merge(obj);

			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}

		return obj;
	}

	@Override
	public void delete(Module obj) {
		EntityManager em = null;
		EntityTransaction tx = null;

		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			em.remove(em.merge(obj));

			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		
	}

	@Override
	public List<Formateur> findFormateurs() {
		List<Formateur> formateurs = new ArrayList<Formateur>();
		formateurs = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;

		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			TypedQuery<Formateur> query = em.createQuery("select Formateur f from Module m", Formateur.class);
			
			formateurs = query.getResultList();
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		
		return formateurs;
	}




	@Override
	public List<Module> findModuleByDate(Date dtDebut, Date dtFin) {

			List<Module> modules = new ArrayList<Module>();
			modules = null;
			
			EntityManager em = null;
			EntityTransaction tx = null;

			try {
				em = Singleton.getInstance().getEmf().createEntityManager();
				tx = em.getTransaction();
				tx.begin();

				TypedQuery<Module> query = em.createQuery("select Module m from Module m where m.dtDebut = :datedebut AND m.dtFin = :datefin", Module.class);
				
				query.setParameter ("datedebut", dtDebut);
				query.setParameter ("datefin", dtFin);
				
				modules = query.getResultList();
				
				tx.commit();
			} catch (Exception e) {
				if (tx != null && tx.isActive()) {
					tx.rollback();
				}
				e.printStackTrace();
			} finally {
				if (em != null) {
					em.close();
				}
			}
			
			return modules;
	}

	@Override
	public List<Module> findModuleByFormation(Formation idFormation) {
		List<Module> modules = new ArrayList<Module>();
		modules = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;

		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();

			TypedQuery<Module> query = em.createQuery("select Module m from Module m where m.idFormation = :idformation", Module.class);
			
			query.setParameter ("idformation", idFormation);

			modules = query.getResultList();
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		
		return modules;
	}



}
