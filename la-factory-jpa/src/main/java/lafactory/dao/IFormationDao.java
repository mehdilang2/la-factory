package lafactory.dao;

import lafactory.model.Formation;

public interface IFormationDao extends IDao<Formation, Long>{

}
