package lafactory.dao;


import java.util.Date;
import java.util.List;

import lafactory.model.Formateur;
import lafactory.model.Formation;
import lafactory.model.Module;


public interface IModuleDao extends IDao<Module,Long>{
	
	List<Formateur> findFormateurs();
	List<Module> findModuleByDate(Date dtDebut, Date dtFin);
	List<Module> findModuleByFormation(Formation idFormation);

}
