package lafactory;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import lafactory.dao.IClientDao;
import lafactory.dao.IClientFormationDao;
import lafactory.dao.IFormationDao;
import lafactory.dao.IMaterielDao;
import lafactory.dao.IMaterielFormationDao;
import lafactory.dao.IMatiereDao;
import lafactory.dao.IModuleDao;
import lafactory.dao.IPersonneDao;
import lafactory.dao.IUtilisateurDao;
import lafactory.dao.jpa.ClientDaoJpa;
import lafactory.dao.jpa.ClientFormationDaoJpa;
import lafactory.dao.jpa.FormationDaoJpa;
import lafactory.dao.jpa.MaterielDaoJpa;
import lafactory.dao.jpa.MaterielFormationDaoJpa;
import lafactory.dao.jpa.MatiereDaoJpa;
import lafactory.dao.jpa.ModuleDaoJpa;
import lafactory.dao.jpa.PersonneDaoJpa;
import lafactory.dao.jpa.UtilisateurDaoJpa;

public class Singleton {

	private static Singleton instance = null;

	private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("la-factory");

	private final IModuleDao moduleDao = new ModuleDaoJpa();
	private final IMatiereDao matiereDao = new MatiereDaoJpa();
	private final IClientFormationDao clientFormationDao = new ClientFormationDaoJpa();
	private final IFormationDao formationDao = new FormationDaoJpa();
	private final IMaterielDao materielDao = new MaterielDaoJpa();
	private final IUtilisateurDao utilisateurDao = new UtilisateurDaoJpa();
	private final IMaterielFormationDao materielFormationDao = new MaterielFormationDaoJpa();
	private final IClientDao clientDao = new ClientDaoJpa();
	private final IPersonneDao personneDao = new PersonneDaoJpa();

	private Singleton() {
	}

	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}

		return instance;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public IModuleDao getModuleDao() {
		return moduleDao;
	}

	public IMatiereDao getMatiereDao() {
		return matiereDao;
	}

	public IClientFormationDao getClientFormationDao() {
		return clientFormationDao;
	}

	public IFormationDao getFormationDao() {
		return formationDao;
	}

	public IMaterielFormationDao getMaterielFormationDao() {
		return materielFormationDao;
	}

	public IMaterielDao getMaterielDao() {
		return materielDao;
	}

	public IUtilisateurDao getUtilisateurDao() {
		return utilisateurDao;
	}

	public IClientDao getClientDao() {
		return clientDao;
	}

	public IPersonneDao getPersonneDao() {
		return personneDao;
	}

}
