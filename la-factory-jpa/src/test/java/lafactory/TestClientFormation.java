package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import lafactory.dao.IClientDao;
import lafactory.dao.IClientFormationDao;
import lafactory.dao.IFormationDao;
import lafactory.model.Adresse;
import lafactory.model.Client;
import lafactory.model.ClientFormation;
import lafactory.model.Formation;
import lafactory.model.TypeEntreprise;

public class TestClientFormation {

	public static void main(String[] args) {
		
		IClientFormationDao clientFormationDao = Singleton.getInstance().getClientFormationDao();
		IClientDao clientDao = Singleton.getInstance().getClientDao();
		IFormationDao formationDao = Singleton.getInstance().getFormationDao();

		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		ClientFormation clientFormation1 = new ClientFormation();
		clientFormation1.setDemandes(5);
		clientFormation1.setCommentaires("Je souhaiterais 5 places pour une formation en Spring, finissant au plus tard fin Septembre");
		clientFormation1 = (ClientFormation) clientFormationDao.save(clientFormation1);
		
		Adresse adresse1 = new Adresse();
		adresse1.setCodePostal("33700");
		adresse1.setComplement("Résidence les renards");
		adresse1.setPays("France");
		adresse1.setVille("Mérignac");
		adresse1.setVoie("2 boulevard Floch");
		
		Client client1 = new Client();
		client1.setNomEntreprise("Sopra Steria");
		client1.setAdresse(adresse1);
		client1.setNumeroSiret("1249359665");
		client1.setNumTva("0.20");
		client1.setTypeEntreprise(TypeEntreprise.SA);
		client1 = (Client) clientDao.save(client1);
		
		Formation formation1 = new Formation();
		formation1.setCapacite(15);
		try {
			formation1.setDtDebut(sdf.parse("19/06/2019"));
			formation1.setDtFin(sdf.parse("20/09/2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		formation1.setIntitule("Angular Spring");
		formation1 = (Formation) formationDao.save(formation1);
		
		Formation formation2 = new Formation();
		formation2.setCapacite(12);
		try {
			formation2.setDtDebut(sdf.parse("21/10/2019"));
			formation2.setDtFin(sdf.parse("20/03/2019"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		formation2.setIntitule("Hibernate");
		formation2 = (Formation) formationDao.save(formation2);

		
		clientFormation1.setFormation(formation1);
		clientFormationDao.save(clientFormation1);
		clientFormation1.setClient(client1);		
		clientFormationDao.save(clientFormation1);
			
		
		
		
		
	}
}
