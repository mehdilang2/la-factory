package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import lafactory.dao.IClientDao;
import lafactory.dao.IClientFormationDao;
import lafactory.dao.IFormateurDao;
import lafactory.dao.IFormationDao;
import lafactory.dao.IMaterielDao;
import lafactory.dao.IMaterielFormationDao;
import lafactory.dao.IMatiereDao;
import lafactory.dao.IModuleDao;
import lafactory.dao.IPersonneDao;
import lafactory.dao.IUtilisateurDao;
import lafactory.model.Adresse;
import lafactory.model.Civilite;
import lafactory.model.Client;
import lafactory.model.ClientFormation;
import lafactory.model.Contact;
import lafactory.model.Disponibilite;
import lafactory.model.Formateur;
import lafactory.model.Formation;
import lafactory.model.Gestionnaire;
import lafactory.model.MaterielFormation;
import lafactory.model.Matiere;
import lafactory.model.Module;
import lafactory.model.Ordinateur;
import lafactory.model.Resolution;
import lafactory.model.Salle;
import lafactory.model.Stagiaire;
import lafactory.model.TypeEntreprise;
import lafactory.model.TypeUser;
import lafactory.model.Utilisateur;
import lafactory.model.Videoprojecteur;

public class TestJPA {

	public static void main(String[] args) {
		EntityManagerFactory emf = Singleton.getInstance().getEmf();
		EntityManager em = null;
		try {
			em = emf.createEntityManager();

			IMaterielDao materielDao = Singleton.getInstance().getMaterielDao();
			IClientFormationDao clientFormationDao = Singleton.getInstance().getClientFormationDao();
			IClientDao clientDao = Singleton.getInstance().getClientDao();
			IFormationDao formationDao = Singleton.getInstance().getFormationDao();
			IModuleDao moduleDao = Singleton.getInstance().getModuleDao();
			IMatiereDao matiereDao = Singleton.getInstance().getMatiereDao();
			IPersonneDao personneDao = Singleton.getInstance().getPersonneDao();
			IUtilisateurDao utilisateurDao = Singleton.getInstance().getUtilisateurDao();
			IMaterielFormationDao materielFormationDao = Singleton.getInstance().getMaterielFormationDao();

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			Salle salle1 = new Salle();
			salle1.setPlaceMax(13);
			salle1.setCode(25L);
			salle1.setCoutUtilisation(56.2f);
			salle1 = (Salle) materielDao.save(salle1);

			Salle salle2 = new Salle();
			salle2.setPlaceMax(50);
			salle2.setCode(26L);
			salle2.setCoutUtilisation(100.99f);
			salle2 = (Salle) materielDao.save(salle2);

			Ordinateur ordi1 = new Ordinateur();
			ordi1.setCode(100L);
			ordi1.setCoutUtilisation(12.5f);
			try {
				ordi1.setDtAchat(sdf.parse("23/07/2019"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			ordi1.setProcesseur("Intel");
			ordi1.setQuantiteDD(8);
			ordi1.setRam(1);
			ordi1 = (Ordinateur) materielDao.save(ordi1);

			Videoprojecteur video = new Videoprojecteur();
			video.setCode(200L);
			video.setCoutUtilisation(2.99f);
			video.setHdmi(true);
			video.setVga(false);
			video.setResolution(Resolution.UltraHaute);
			video = (Videoprojecteur) materielDao.save(video);

			Adresse adresse = new Adresse();
			adresse.setVoie("86 rue J.F. Kennedy");
			adresse.setVille("Merignac");
			adresse.setCodePostal("33700");
			adresse.setPays("France");

			salle1.setAdresse(adresse);
			salle1 = (Salle) materielDao.save(salle2);

			MaterielFormation materielformation = new MaterielFormation();
			materielformation.setDtDebut(sdf.parse("18/06/2019"));
			materielformation.setDtDebut(sdf.parse("20/09/2019"));
			
			materielformation = materielFormationDao.save(materielformation);
			
			
			// Client formation
			ClientFormation clientFormation1 = new ClientFormation();
			clientFormation1.setDemandes(5);
			clientFormation1.setCommentaires("Je souhaiterais 5 places pour une formation en Spring, finissant au plus tard fin Septembre");
			clientFormation1 = clientFormationDao.save(clientFormation1);
			
			Adresse adresse1 = new Adresse();
			adresse1.setCodePostal("33700");
			adresse1.setComplement("Résidence les renards");
			adresse1.setPays("France");
			adresse1.setVille("Mérignac");
			adresse1.setVoie("2 boulevard Floch");
			
			
			Client client1 = new Client();
			client1.setNomEntreprise("Sopra Steria");
			client1.setAdresse(adresse1);
			client1.setNumeroSiret("1249359665");
			client1.setNumTva("0.20");
			client1.setTypeEntreprise(TypeEntreprise.SA);
			
			client1 = clientDao.save(client1);
			
			Formation formation1 = new Formation();
			formation1.setCapacite(15);
			try {
				formation1.setDtDebut(sdf.parse("19/06/2019"));
				formation1.setDtFin(sdf.parse("20/09/2019"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			formation1.setIntitule("Angular Spring");
			
			Formation formation2 = new Formation();
			formation2.setCapacite(12);
			try {
				formation2.setDtDebut(sdf.parse("21/10/2019"));
				formation2.setDtFin(sdf.parse("20/03/2019"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			formation2.setIntitule("Hibernate");
			
			List<Formation> formations = new ArrayList<Formation>();
			formations.add(formation1);
			formations.add(formation2);
			
			formation1 = formationDao.save(formation1);
			formation2 = formationDao.save(formation2);
			
			
			//Formation
			Formation antoinette = new Formation();
			antoinette.setCapacite(12);
			antoinette.setIntitule("FORMATION JAVA POEI");
			try {
				antoinette.setDtDebut(sdf.parse("19/06/2019"));
				antoinette.setDtFin(sdf.parse("20/09/2019"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			antoinette = formationDao.save(antoinette);
			
			materielformation.setFormation(antoinette);
			
			materielFormationDao.save(materielformation);
			
			Module javaEE = new Module();
			
			try {
				javaEE.setDtDebut(sdf.parse("19/06/2019"));
				javaEE.setDtFin(sdf.parse("30/06/2019"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			javaEE.setFormation(antoinette);
			
			javaEE = moduleDao.save(javaEE);
			
			Matiere java = new Matiere(); 
			java.setContenu("test");
			java.setDuree(50);
			java.setObjectifs("oui");
			java.setPrerequis("QI 300");
			java.setTitre("On est bien");
			
			
			java = matiereDao.save(java);
			
			javaEE.setMatiere(java);
			
			javaEE = moduleDao.save(javaEE);
			
			java.setModule(javaEE);
			
			java = matiereDao.save(java);
			
			// Personne
			Adresse adresseCamille = new Adresse();
			adresseCamille.setCodePostal("33200");
			adresseCamille.setPays("France");
			adresseCamille.setVille("Bordeaux");
			adresseCamille.setVoie("18 bis rue de la Paix");

			Adresse adresse2 = new Adresse();
			adresse2.setCodePostal("33200");
			adresse2.setPays("France");
			adresse2.setVille("Bordeaux");
			adresse2.setVoie("41 bis rue de la Paix");

			Client sopra = new Client();
			sopra.setAdresse(adresseCamille);
			sopra.setNomEntreprise("Sopra");
			sopra.setNumeroSiret("ede1554543354");
			sopra.setNumTva("fzjek456486415616");
			sopra.setTypeEntreprise(TypeEntreprise.SAS);

			sopra = clientDao.save(sopra);

			Utilisateur laurie = new Utilisateur();
			laurie.setTypeUser(TypeUser.Client);
			laurie.setMail("laurie.avignon@soprasteria.com");
			laurie.setMotDePasse("soprasteria");

			laurie = utilisateurDao.save(laurie);

			Utilisateur eric = new Utilisateur();
			eric.setTypeUser(TypeUser.Gestionnaire);
			eric.setMail("eric.sultan@ajc.com");
			eric.setMotDePasse("ILovePatrick");

			eric = utilisateurDao.save(eric);

			Utilisateur john = new Utilisateur();
			john.setTypeUser(TypeUser.Administrateur);
			john.setMail("john.doe@mail.com");
			john.setMotDePasse("JohnDoe");

			john = utilisateurDao.save(john);

			Utilisateur olivier = new Utilisateur();
			olivier.setTypeUser(TypeUser.Formateur);
			olivier.setMail("olivier@formateur.com");
			olivier.setMotDePasse("OlivierFormateur");


			olivier = utilisateurDao.save(olivier);

			Stagiaire camille = new Stagiaire();
			camille.setCivilite(Civilite.MLLE);
			camille.setAdresse(adresseCamille);
//			camille.setClient(sopra);
			camille.setCommentaires("débutant");
//			camille.setFormation(menagerie);
			camille.setNom("Saphy");
			camille.setPrenom("Camille");
			camille.setTelephone("0617227456");

			camille = (Stagiaire) personneDao.save(camille);

			Contact contactSopra = new Contact();
			contactSopra.setAdresse(adresse2);
			contactSopra.setCivilite(Civilite.MME);
			contactSopra.setNom("Nemar");
			contactSopra.setPrenom("Jean");
			contactSopra.setTelephone("0614578965");
			contactSopra.setUtilisateur(laurie);

			contactSopra = (Contact) personneDao.save(contactSopra);

			Gestionnaire paul = new Gestionnaire();
			paul.setAdresse(adresse2);
			paul.setCivilite(Civilite.M);
			paul.setNom("Sultan");
			paul.setPrenom("Eric");
			paul.setTelephone("0645874894");
			paul.setUtilisateur(eric);
//			eric.getFormations().add(menagerie);

			paul = (Gestionnaire) personneDao.save(paul);

			Disponibilite dispo = new Disponibilite();
			try {
				dispo.setDtDispo(sdf.parse("01/12/2019"));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
			Formateur patrick = new Formateur();
			patrick.setNom("SULTAN");
			patrick.setPrenom("eric");
			patrick.setCivilite(Civilite.M);
			patrick.setDisponibilite(dispo);
			patrick.setAdresse(adresse2);
//		       eric.getMatieres().add(javaDebutant);
			patrick.setAdresse(adresse2);
			patrick.setTelephone("0606456849");
			patrick.setUtilisateur(olivier);
//		       eric.getModules().add(module1)
			patrick = (Formateur) personneDao.save(patrick);
//			sopra.getClientFormations().add(e);
			sopra.getContacts().add(contactSopra);
			sopra.getStagiaires().add(camille);
			
			sopra = clientDao.save(sopra);
					
			laurie.setPersonne(contactSopra);
			
			laurie = utilisateurDao.save(laurie);
			
			eric.setPersonne(paul);
			
			eric = utilisateurDao.save(eric);
			
			olivier.setPersonne(patrick);
			
			olivier = utilisateurDao.save(olivier);
			
			clientFormation1.setClient(client1);
			clientFormation1.setFormation(formation1);

			clientFormationDao.save(clientFormation1);
			
			javaEE.setFormateur(patrick);
			moduleDao.save(javaEE);
			
			List<Formateur> formateurs = new ArrayList<Formateur>();
			
			formateurs.add(patrick);
			
			java.setFormateurs(formateurs);
			
			matiereDao.save(java);
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}

			emf.close();
		}
	}

}
