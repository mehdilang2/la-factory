package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import lafactory.dao.IClientDao;
import lafactory.dao.IPersonneDao;
import lafactory.dao.IUtilisateurDao;
import lafactory.model.Adresse;
import lafactory.model.Civilite;
import lafactory.model.Client;
import lafactory.model.Contact;
import lafactory.model.Disponibilite;
import lafactory.model.Formateur;
import lafactory.model.Gestionnaire;
import lafactory.model.Stagiaire;
import lafactory.model.TypeEntreprise;
import lafactory.model.TypeUser;
import lafactory.model.Utilisateur;

public class TestJpaPersonne {

	public static void main(String[] args) {
		IPersonneDao personneDao = Singleton.getInstance().getPersonneDao();
		IUtilisateurDao utilisateurDao = Singleton.getInstance().getUtilisateurDao();
		IClientDao clientDao = Singleton.getInstance().getClientDao();

		Adresse adresseCamille = new Adresse();
		adresseCamille.setCodePostal("33200");
		adresseCamille.setPays("France");
		adresseCamille.setVille("Bordeaux");
		adresseCamille.setVoie("18 bis rue de la Paix");

		Adresse adresse2 = new Adresse();
		adresse2.setCodePostal("33200");
		adresse2.setPays("France");
		adresse2.setVille("Bordeaux");
		adresse2.setVoie("41 bis rue de la Paix");

		Client sopra = new Client();
		sopra.setAdresse(adresseCamille);
		sopra.setNomEntreprise("Sopra");
		sopra.setNumeroSiret("ede1554543354");
		sopra.setNumTva("fzjek456486415616");
		sopra.setTypeEntreprise(TypeEntreprise.SAS);

		sopra = clientDao.save(sopra);

		Utilisateur laurie = new Utilisateur();
		laurie.setTypeUser(TypeUser.Client);
		laurie.setMail("laurie.avignon@soprasteria.com");
		laurie.setMotDePasse("soprasteria");

		laurie = utilisateurDao.save(laurie);

		Utilisateur eric = new Utilisateur();
		eric.setTypeUser(TypeUser.Gestionnaire);
		eric.setMail("eric.sultan@ajc.com");
		eric.setMotDePasse("ILovePatrick");

		eric = utilisateurDao.save(eric);

		Utilisateur john = new Utilisateur();
		john.setTypeUser(TypeUser.Administrateur);
		john.setMail("john.doe@mail.com");
		john.setMotDePasse("JohnDoe");

		john = utilisateurDao.save(john);

		Utilisateur olivier = new Utilisateur();
		olivier.setTypeUser(TypeUser.Formateur);
		olivier.setMail("olivier@formateur.com");
		olivier.setMotDePasse("OlivierFormateur");


		olivier = utilisateurDao.save(olivier);

		Stagiaire camille = new Stagiaire();
		camille.setCivilite(Civilite.MLLE);
		camille.setAdresse(adresseCamille);
//		camille.setClient(sopra);
		camille.setCommentaires("débutant");
//		camille.setFormation(menagerie);
		camille.setNom("Saphy");
		camille.setPrenom("Camille");
		camille.setTelephone("0617227456");

		camille = (Stagiaire) personneDao.save(camille);

		Contact contactSopra = new Contact();
		contactSopra.setAdresse(adresse2);
		contactSopra.setCivilite(Civilite.MME);
		contactSopra.setNom("Nemar");
		contactSopra.setPrenom("Jean");
		contactSopra.setTelephone("0614578965");
		contactSopra.setUtilisateur(laurie);

		contactSopra = (Contact) personneDao.save(contactSopra);

		Gestionnaire paul = new Gestionnaire();
		paul.setAdresse(adresse2);
		paul.setCivilite(Civilite.M);
		paul.setNom("Sultan");
		paul.setPrenom("Eric");
		paul.setTelephone("0645874894");
		paul.setUtilisateur(eric);
//		eric.getFormations().add(menagerie);

		paul = (Gestionnaire) personneDao.save(paul);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Disponibilite dispo = new Disponibilite();
		try {
			dispo.setDtDispo(sdf.parse("01/12/2019"));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		Formateur patrick = new Formateur();
		patrick.setNom("SULTAN");
		patrick.setPrenom("eric");
		patrick.setCivilite(Civilite.M);
		patrick.setDisponibilite(dispo);
		patrick.setAdresse(adresse2);
//	       eric.getMatieres().add(javaDebutant);
		patrick.setAdresse(adresse2);
		patrick.setTelephone("0606456849");
		patrick.setUtilisateur(olivier);
//	       eric.getModules().add(module1)

//		sopra.getClientFormations().add(e);
		sopra.getContacts().add(contactSopra);
		sopra.getStagiaires().add(camille);
		
		sopra = clientDao.save(sopra);
				
		laurie.setPersonne(contactSopra);
		
		laurie = utilisateurDao.save(laurie);
		
		eric.setPersonne(paul);
		
		eric = utilisateurDao.save(eric);
		
		olivier.setPersonne(patrick);
		
		olivier = utilisateurDao.save(olivier);
	}

}
