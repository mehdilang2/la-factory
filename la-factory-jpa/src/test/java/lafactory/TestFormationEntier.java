package lafactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import lafactory.dao.IClientFormationDao;
import lafactory.dao.IFormationDao;
import lafactory.dao.IMatiereDao;
import lafactory.dao.IModuleDao;
import lafactory.model.ClientFormation;
import lafactory.model.Formation;
import lafactory.model.Matiere;
import lafactory.model.Module;

public class TestFormationEntier {

	public static void main(String[] args) {
		
		IFormationDao formationDao = Singleton.getInstance().getFormationDao();
		IModuleDao moduleDao = Singleton.getInstance().getModuleDao();
		IMatiereDao matiereDao = Singleton.getInstance().getMatiereDao();
		IClientFormationDao clientFormationDao = Singleton.getInstance().getClientFormationDao();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		
		Formation antoinette = new Formation();
		antoinette.setCapacite(12);
		antoinette.setIntitule("FORMATION JAVA POEI");
		try {
			antoinette.setDtDebut(sdf.parse("19/06/2019"));
			antoinette.setDtFin(sdf.parse("20/09/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		antoinette = formationDao.save(antoinette);
		
		Module javaEE = new Module();
		
		try {
			javaEE.setDtDebut(sdf.parse("19/06/2019"));
			javaEE.setDtFin(sdf.parse("30/06/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		javaEE.setFormation(antoinette);
		
		javaEE = moduleDao.save(javaEE);
		
		Matiere java = new Matiere(); 
		java.setContenu("test");
		java.setDuree(50);
		java.setObjectifs("oui");
		java.setPrerequis("QI 300");
		java.setTitre("On est bien");
		
		
		java = matiereDao.save(java);
		
		javaEE.setMatiere(java);
		
		javaEE = moduleDao.save(javaEE);
		
		java.setModule(javaEE);
		
		java = matiereDao.save(java);		
		
		
//		javaEE = moduleDao.save(javaEE);
//		
//		
//		ClientFormation clientFormation = new ClientFormation();
//		clientFormation.setCommentaires("j'adore les sushis");
//		clientFormation.setDemandes(5);
//		clientFormation.setFormation(antoinette);
//		clientFormation.setStatut(true);
//		
//		clientFormation = clientFormationDao.save(clientFormation);
		
		
		
	}

}
