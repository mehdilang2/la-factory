package lafactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import lafactory.dao.IFormateurDao;
import lafactory.dao.IMatiereDao;
import lafactory.dao.IModuleDao;
import lafactory.model.Formateur;
import lafactory.model.Matiere;
import lafactory.model.Module;


public class TestMatiere {

	public static void main(String[] args) {
		
		IMatiereDao matiereDao = Singleton.getInstance().getMatiereDao();
		IModuleDao moduleDao = Singleton.getInstance().getModuleDao();
		//IFormateurDao formateurDao = Singleton.getInstance().getFormateurDao();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Matiere javaDebutant = null;
		Formateur formateur1 = null;
		Formateur formateur2 = null;
		Module module = null;
		List<Formateur> formateurs = new ArrayList<Formateur>();
		
		javaDebutant = new Matiere();
		javaDebutant.setTitre("JAVA Débutant");
		javaDebutant.setDuree(3); // Jour ou mois??
		javaDebutant.setObjectifs("Apprendre à programmer les bases de JAVA");
		javaDebutant.setPrerequis("Aucun");
		javaDebutant.setContenu("Présentation JAVA8 \n Outils de développement \n Création de classes");
		
		formateur1.setNom("Rocket");
		formateur2.setNom("Belette");
		formateurs.add(formateur1);
		formateurs.add(formateur2);
		javaDebutant.setFormateurs(formateurs);
		
		try {
		module.setDtDebut(sdf.parse("19/06/2019"));
		module.setDtFin(sdf.parse("20/09/2019"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		matiereDao.save(javaDebutant);
		moduleDao.save(module);
		//formateurDao.save(formateur1);
		//formateurDao.save(formateur2);
		
	}

}
