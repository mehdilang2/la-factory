package lafactory;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import lafactory.dao.IMaterielDao;
import lafactory.model.Adresse;
import lafactory.model.MaterielFormation;
import lafactory.model.Ordinateur;
import lafactory.model.Resolution;
import lafactory.model.Salle;
import lafactory.model.Videoprojecteur;

public class TestJpaMateriel {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Singleton.getInstance().getEmf();
		EntityManager em = null;
		EntityTransaction tx = null;
		try {
			em = emf.createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			
			IMaterielDao materielDao = Singleton.getInstance().getMaterielDao();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
			Salle salle1= new Salle();
			salle1.setPlaceMax(13);
			salle1.setCode(25L);
			salle1.setCoutUtilisation(56.2f);
			salle1 = (Salle) materielDao.save(salle1);
			
			Salle salle2= new Salle();
			salle2.setPlaceMax(50);
			salle2.setCode(26L);
			salle2.setCoutUtilisation(100.99f);
			salle2 = (Salle) materielDao.save(salle2);
			
			Ordinateur ordi1 = new Ordinateur();
			ordi1.setCode(100L);
			ordi1.setCoutUtilisation(12.5f);
			try {
			ordi1.setDtAchat(sdf.parse("23/07/2019"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			ordi1.setProcesseur("Intel");
			ordi1.setQuantiteDD(8);
			ordi1.setRam(1);
			ordi1 = (Ordinateur) materielDao.save(ordi1);
			
			Videoprojecteur video = new Videoprojecteur();
			video.setCode(200L);
			video.setCoutUtilisation(2.99f);
			video.setHdmi(true);
			video.setVga(false);
			video.setResolution(Resolution.UltraHaute);
			video = (Videoprojecteur) materielDao.save(video);
			
			Adresse adresse = new Adresse();
			adresse.setVoie("86 rue J.F. Kennedy");
			adresse.setVille("Merignac");
			adresse.setCodePostal("33700");
			adresse.setPays("France");
			
			salle1.setAdresse(adresse);
			salle1 = (Salle) materielDao.save(salle2);
			
			MaterielFormation materielformation = new MaterielFormation();
			materielformation.setDtDebut(sdf.parse("18/06/2019"));
			materielformation.setDtDebut(sdf.parse("20/09/2019"));
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
			
			emf.close();
		}
	}

}
